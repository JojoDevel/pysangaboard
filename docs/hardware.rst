Sangaboard hardware
=====================
This module controls the Sangaboard.

The Sangaboard is an open-source Motor controller based on the Arduino
project. We currently support two versions of the board:

The v0.3 board which is a motorboard based on an ATmega32U4 which can be
programmed in the Arduino IDE as an Arduino Leonardo (or the IDE can
be customised to have a Sangaboard option). The board is open source
and available from `GitLab <https://gitlab.com/bath_open_instrumentation_group/sangaboard/tree/master>`__
The board is completely surface mount so it is somewhat difficult to make. We are in the
process of working out how to distribute it via Seeed Studio. You can order the boards
via `Kitspace <https://kitspace.org/boards/gitlab.com/bath_open_instrumentation_group/sangaboard/>`__.

The legacy v0.2 board which connects to an Arduino Nano and some
Darlington pair ICs.  The PCB design is available on Gitlab,
`Gitlab <https://gitlab.com/bath_open_instrumentation_group/sangaboard/tree/Sangaboard-v0.2>`__
and the boards can be ordered from `Kitspace`_ (under the legacy name openflexure_nano_motor_controller).
You can also build a circuit equivalent to the v0.2 using the motor drivers that come with
the 28BYJ-48 motors and an Arduino Nano using
`these instructions <https://build.openflexure.org/openflexure-microscope/v6.0.0/docs/#/6_motor_controllers?id=simple-controller-using-arduino-nano>`__.

.. _Kitspace: https://kitspace.org/boards/github.com/rwb27/openflexure_nano_motor_controller/

